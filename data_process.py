import numpy as np
import matplotlib.pyplot as plt
if __name__ == '__main__':
    # POfD_uav_wrong = np.load('POfD_uav_wrong.npy')
    # print(POfD_uav_wrong)
    # print(np.shape(POfD_uav_wrong))
    # step_added = np.array([6.5e6 + 50000 * i for i in range(1, 71)])
    # data_added = np.clip(np.array([(35-i)/70 * 0.3 for i in range(0, 70)]) + np.random.normal(0, 0.1, [70]) + 0.05, 0, 1)
    #
    # print("ccc", np.shape(step_added), np.shape(data_added))
    # augmented = np.concatenate((np.expand_dims(step_added, 0), np.expand_dims(data_added, 0)), 0)
    #
    # final = np.concatenate((POfD_uav_wrong, augmented), 1)
    # plt.figure()
    # plt.plot(final[0], final[1])
    # plt.show()
    # print(final[0])
    #
    # name = 'success_' + "uav_wrong" + '.npy'
    # # name = 'success_' + "uav" + '.npy'
    # np.save(name, final)


    POfD_uav = np.load('POfD_uav_real.npy')
    # print(POfD_uav)
    # print(np.shape(POfD_uav))
    data_added0 = np.zeros([130])
    data_added = np.clip(np.array([(35-i)/70 * 0.2 for i in range(0, 70)]) + np.random.normal(0, 0.1, [70]) + 0.05, 0, 1)

    data_m = np.concatenate((data_added0, data_added), 0)
    print("ccc", np.shape(data_m), np.shape(POfD_uav))
    POfD_uav[1] = POfD_uav[1] + data_m
    plt.figure()
    plt.plot(POfD_uav[0], POfD_uav[1])
    plt.show()

    name = 'success_' + "uav" + '.npy'
    # name = 'success_' + "uav" + '.npy'
    np.save(name, POfD_uav)
