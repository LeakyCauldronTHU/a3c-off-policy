import numpy as np

import matplotlib.pyplot as plt


x = np.linspace(1, 1e6, 1000000)

# y = (4 * np.log(1.0 + 0.0001 * x)) ** 2
y = (4 * np.log(1.0 + 0.0001 * x)) ** 2
plt.plot(x, y)

y = (4 * np.log(1.0 + 0.0001 * (np.sign(x) + 1.0) / 2.0 * x) + 1) ** 2
plt.plot(x, y)

y = 0.0005 * x
plt.plot(x, y)

plt.show()
# (torch.log(1.0+0.0001*(torch.sign(time_step)+1.0)/2.0*time_step)*4+1).pow(2)