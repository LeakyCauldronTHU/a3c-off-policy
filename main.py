from __future__ import print_function, division
import os
os.environ["OMP_NUM_THREADS"] = "1"
import argparse
import torch
import torch.multiprocessing as mp
from environment import create_env
from model import A3C_MLP
from train import train, train_pre
from test import test, test_pre
from shared_optim import SharedRMSprop, SharedAdam
import time
import datetime
import os.path as osp
from utils import str_process
import subprocess
import gym
import gym_uav


parser = argparse.ArgumentParser(description='A3C')
parser.add_argument(
    '--lr',
    type=float,
    default=0.0001,
    metavar='LR',
    help='learning rate (default: 0.0001)')
parser.add_argument(
    '--gamma',
    type=float,
    default=0.99,
    metavar='G',
    help='discount factor for rewards (default: 0.99)')
parser.add_argument(
    '--tau',
    type=float,
    default=1.00,
    metavar='T',
    help='parameter for GAE (default: 1.00)')
parser.add_argument(
    '--seed',
    type=int,
    default=1,
    metavar='S',
    help='random seed (default: 1)')
parser.add_argument(
    '--env',
    default='uav-v0',
    metavar='ENV',
    help='environment to train on (default: UAV)')
parser.add_argument(
    '--workers',
    type=int,
    default=16,
    metavar='W',
    help='how many training processes to use (default: 32)')
parser.add_argument(
    '--num-steps',
    type=int,
    default=20,
    metavar='NS',
    help='number of forward steps in A3C (default: 300)')
parser.add_argument(
    '--max-episode-length',
    type=int,
    default=2000,
    metavar='M',
    help='maximum length of an episode (default: 2000)')

parser.add_argument(
    '--variance',
    type=float,
    nargs='+',
    metavar='V',
    help='variance of actions')

parser.add_argument(
    '--prior-decay',
    type=float,
    default=0.0005,
    metavar='PD',
    help='decay slope of prior (default: 0.0005)')

parser.add_argument(
    '--shared-optimizer',
    default=True,
    metavar='SO',
    help='use an optimizer without shared statistics.')
parser.add_argument(
    '--load',
    default=False,
    metavar='L',
    help='load a trained model')
parser.add_argument(
    '--save-max',
    default=True,
    metavar='SM',
    help='Save model on every test run high score matched or bested')
parser.add_argument(
    '--optimizer',
    default='Adam',
    metavar='OPT',
    help='shares optimizer choice of Adam or RMSprop')
parser.add_argument(
    '--load-model-dir',
    default='trained_models/',
    metavar='LMD',
    help='folder to load trained models from')
parser.add_argument(
    '--save-model-dir',
    default='trained_models/',
    metavar='SMD',
    help='folder to save trained models')
parser.add_argument(
    '--log-dir',
    default='//philly/gcr/resrchvc/v-wancha/logs/a3c-off-policy/',
    metavar='LG',
    help='folder to save logs')
parser.add_argument(
    '--model',
    default='MLP',
    metavar='M',
    help='Model type to use')
parser.add_argument(
    '--stack-frames',
    type=int,
    default=1,
    metavar='SF',
    help='Choose number of observations to stack')
parser.add_argument(
    '--amsgrad',
    default=True,
    metavar='AM',
    help='Adam optimizer amsgrad parameter')

parser.add_argument(
    '--pretrain',
    default=False,
    metavar='PT',
    help='Pre-train the model or not')

parser.add_argument(
    '--supervised-steps',
    type=int,
    default=int(1E6))

parser.add_argument(
    '--training-steps',
    type=int,
    default=int(1.5E7))

parser.add_argument(
    '--l2-regular',
    type=int,
    default=1e-5)

parser.add_argument(
    '--test-episodes',
    type=int,
    default=50,
    metavar='TE',
    help='number of episodes used for testing the trained policy (default: 100)')

parser.add_argument(
    '--cache-interval',
    type=int,
    default=50000,
    metavar='CI',
    help='each interval, cache model into queue (default: 10000)')

parser.add_argument('--reward-type', type=str, default="sparse", help='environment reward type')
parser.add_argument('--demo-type', type=str, default='uav', help='demonstration type, available choices are (uav, uav_wrong, else)')
parser.add_argument('--use-prior', default=False, action='store_true')
parser.add_argument('--cluster', default=None, type=str)
parser.add_argument('--no-decay', default=False, action='store_true')

# Based on
# https://github.com/pytorch/examples/tree/master/mnist_hogwild
# Training settings
# Implemented multiprocessing using locks but was not beneficial. Hogwild
# training was far superior


def delete_old_logs(dir, logs):
    try:
        existing_logs = os.listdir(dir)
        for lg in existing_logs:
            if logs in lg:
                print("we have found old logs", lg, logs)
                cmd = 'rm -rf ' + os.path.join(dir, lg)
                print(cmd)
                subprocess.call(cmd, shell=True)

    except:
        print("there are no old files")
        pass


if __name__ == '__main__':
    args = parser.parse_args()
    args.seed = args.seed * 1201
    torch.manual_seed(args.seed)
    env = create_env(args.env, -1)

    shared_model = A3C_MLP(env.observation_space, env.action_space, args.stack_frames)
    shared_model.share_memory()

    args_dict = args.__dict__
    dir_tmp = ''
    if args.log_dir is None:
        args.log_dir = '~'

    if args.cluster:
        args.log_dir = r'//philly/' + args.cluster + r'/resrchvc/v-wancha/logs_a3c-off-policy-trans'
    else:
        # args.log_dir = r'//philly/gcr/resrchvc/v-wancha/logs/a3c-off-policy'
        args.log_dir = 'log'

    if not args.use_prior:
        infos_args = ['env', 'use_prior', 'seed']

    else:
        infos_args = ['env', 'use_prior', 'prior_decay', 'variance', 'demo_type', 'seed']
        if args.no_decay:
            infos_args += ['no_decay']
    for i in range(len(infos_args)):
        tmp = str(args_dict[infos_args[i]])
        print(tmp, type(tmp))
        if '[' in tmp:
            tmp = (tmp[1:])[:-1]
        if ' ' in tmp:
            tmp = tmp.split(', ')[0] + "_" + tmp.split(' ')[1]
        dir_tmp = dir_tmp + str_process(infos_args[i]) + '=' + tmp + '-'
    dir_tmp = dir_tmp.replace('.', '_')

    delete_old_logs(args.log_dir, dir_tmp)

    dir_tmp = dir_tmp + datetime.datetime.now().strftime("%m_%d_%H_%M_%S_%f")
    # print("tem", args.log_dir, dir_tmp)
    args.log_dir = osp.join(args.log_dir, dir_tmp)

    if args.log_dir[0] == '~':
        args.log_dir = osp.expanduser(args.log_dir)
    args.log_dir = args.log_dir + '/'
    print(args.log_dir)
    # exit(0)

    os.makedirs(args.log_dir, exist_ok=True)

    if args.load:
        saved_state = torch.load('{0}{1}.dat'.format(
            args.load_model_dir, args.env), map_location=lambda storage, loc: storage)
        shared_model.load_state_dict(saved_state)

    # optimizer for policy and value optimization
    if args.shared_optimizer:
        if args.optimizer == 'RMSprop':
            optimizer = SharedRMSprop(shared_model.parameters(), lr=args.lr)
        if args.optimizer == 'Adam':
            optimizer = SharedAdam(
                shared_model.parameters(), lr=args.lr, amsgrad=args.amsgrad)
        optimizer.share_memory()
    else:
        optimizer = None

    # optimizer for pre-training policy and value models
    if args.shared_optimizer:
        if args.optimizer == 'RMSprop':
            optimizer_pre = SharedRMSprop(shared_model.parameters(), lr=args.lr)
        if args.optimizer == 'Adam':
            optimizer_pre = SharedAdam(
                shared_model.parameters(), lr=args.lr, amsgrad=args.amsgrad)
        optimizer_pre.share_memory()
    else:
        optimizer_pre = None

    # pre-train model use demonstrations
    if args.pretrain:
        if args.load:
            saved_state = torch.load('{0}{1}_pre.dat'.format(
                args.load_model_dir, args.env), map_location=lambda storage, loc: storage)
            shared_model.load_state_dict(saved_state)
        print("load pre-trained model")
        processes = []
        p = mp.Process(target=test_pre, args=(-1, args, shared_model))
        p.start()
        processes.append(p)
        time.sleep(0.1)
        for rank in range(1, args.workers+1):
            p = mp.Process(target=train_pre, args=(
                rank, args, shared_model, optimizer_pre))
            p.start()
            processes.append(p)
            time.sleep(0.1)

        for p in processes:
            time.sleep(0.1)
            p.join()

    if args.load or args.pretrain:
        saved_state = torch.load('{0}{1}.dat'.format(
            args.load_model_dir, args.env), map_location=lambda storage, loc: storage)
        shared_model.load_state_dict(saved_state)

    # reinforcement learning with experience
    processes = []
    p = mp.Process(target=test, args=(-1, args, shared_model))
    p.start()
    processes.append(p)
    time.sleep(0.1)
    for rank in range(0, args.workers):
        p = mp.Process(target=train, args=(
            rank, args, shared_model, optimizer))
        p.start()
        processes.append(p)
        time.sleep(0.1)
        
    for p in processes:
        time.sleep(0.1)
        p.join()
