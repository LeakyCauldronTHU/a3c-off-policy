# coding: utf-8
import numpy as np
from gym.spaces import Box
import gym


class EnvUAV(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):
        self.observation_space = Box(-np.inf, np.inf, [15 + 7], float)
        self.action_space = Box(-1.0, 1.0, [2], float)
        self._max_episode_steps = 10000
        self.env_step_counter = 0
        self.reward_type = "dense"
        assert self.reward_type == "dense" or self.reward_type == "sparse"
        self.File = open('./Result/path_agent.txt', 'w+')

        # 无人机参数
        self.level = 100.0  # 定义无人机的飞行高度
        self.scope = 100.0  # 定义无人机传感器的视野范围
        self.state = np.zeros([15 + 7])  # 定义无人机状态向量并随机初始化
        self.position = np.zeros([2])
        self.target = np.zeros([2])
        self.orient = np.zeros([1])
        self.speed = np.zeros([1])  # 定义无人机的初始速度为0.0

        # 环境参数
        self.expand = 32
        self.num_circle = 10  # basic 环境重复次数
        self.min_step = 0.1  # 定义无人机传感器搜索步长
        self.radius = 60  # 障碍物的半径
        self.repeat = 200  # 环境重复的周期
        self.mat_height = np.random.randint(
            1, 10, size=(self.num_circle + 2 * self.expand, self.num_circle + 2 * self.expand))  # 环境中建筑物的高度
        self.mat_exist = self.mat_height - self.level  # 行高度范围内的建筑

    def obtain_state(self, position, target, orient):
        for i in range(0, 9):
            theta = np.mod((i - 4) * np.pi / 8 + orient, 2 * np.pi)  # 计算传感器各个方向的角度,保证不超过360度

            end_cache = position

            end = np.mod(end_cache, self.repeat)
            Count = 0
            while True:
                Count = Count + 1
                position_integer = (end_cache / self.repeat).astype(np.int)
                if self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand] > 0:
                    if np.linalg.norm(end - np.array([self.repeat / 2, self.repeat / 2])) - self.radius <= 0:
                        self.state[i] = np.linalg.norm(end_cache - position) / self.scope
                        break
                if Count == 1000:
                    self.state[i] = 1
                    break
                end_cache = end_cache + np.array([self.min_step * np.sin(theta[0]), self.min_step * np.cos(theta[0])])
                end = np.mod(end_cache, self.repeat)

        extra_counter = 0
        for i in [-12, -3, -1, 1, 3, 12, 16]:
            extra_counter += 1
            theta = np.mod(i * np.pi / 16 + orient, 2 * np.pi)  # 计算传感器各个方向的角度,保证不超过360度

            end_cache = position

            end = np.mod(end_cache, self.repeat)
            Count = 0
            while True:
                Count = Count + 1
                position_integer = (end_cache / self.repeat).astype(np.int)
                if self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand] > 0:
                    if np.linalg.norm(end - np.array([self.repeat / 2, self.repeat / 2])) - self.radius <= 0:
                        self.state[14 + extra_counter] = np.linalg.norm(end_cache - position) / self.scope
                        break
                if Count == 1000:
                    self.state[14 + extra_counter] = 1
                    break
                end_cache = end_cache + np.array([self.min_step * np.sin(theta[0]), self.min_step * np.cos(theta[0])])
                end = np.mod(end_cache, self.repeat)

        #################################################################################################

        dist = np.linalg.norm(target - position)

        self.state[9] = 2 / (np.exp(-0.002 * dist) + 1) - 1
        # 计算目标和当前位置的相对夹角
        theta_target = np.arctan((target[0] - position[0]) / (target[1] - position[1]))
        if (target[0] >= position[0]) and (target[1] >= position[1]):
            self.state[10] = np.sin(theta_target)
            self.state[11] = np.cos(theta_target)
        elif target[1] < position[1]:
            self.state[10] = np.sin(theta_target + np.pi)
            self.state[11] = np.cos(theta_target + np.pi)
        else:
            self.state[10] = np.sin(theta_target + 2 * np.pi)
            self.state[11] = np.cos(theta_target + 2 * np.pi)

        # 保存目标的绝对方向角
        self.state[12] = np.sin(orient)  # normalization
        self.state[13] = np.cos(orient)  # normalization
        # 保存速度为另一个状态
        self.state[14] = np.tanh(0.1 * self.speed)

    def reset(self, mode='human', close=False):
        self.env_step_counter = 0
        self.mat_height = np.random.randint(
            1, 10, size=(self.num_circle + 2 * self.expand, self.num_circle + 2 * self.expand)) * 17.0 + 30.0  # 建筑物高度
        self.mat_exist = self.mat_height - self.level  # 确定飞行高度范围内的建筑
        while True:
            position = np.random.uniform(0, self.repeat, size=(2,))
            if np.linalg.norm(position - np.array([self.repeat / 2, self.repeat / 2])) - self.radius > 0:
                break
        relative_position = np.random.randint(0, self.num_circle, size=(2,)).astype(np.float)
        self.position = position + relative_position * self.repeat

        # ##############################其次产生目标位置#####################################

        while True:
            target = np.random.uniform(0, self.repeat, size=(2,))
            if np.linalg.norm(target - np.array([self.repeat / 2, self.repeat / 2])) - self.radius > 0:
                break

        while True:  # ensure that the minimum distance between initial position and target position is larger than 200m
            relative_target = np.random.randint(0, self.num_circle, size=(2,)).astype(np.float)
            target_temp = np.array(target + relative_target * self.repeat)
            if np.linalg.norm(target_temp - self.position) >= 200.0:
                break
        self.target = target + relative_target * self.repeat
        #################################################################################################
        self.orient = np.random.uniform(0, 2 * np.pi, size=(1,))
        self.speed = np.zeros([1])

        #################################################################################################
        np.savetxt('./Result/mat_height.txt', self.mat_height, fmt='%d', delimiter=' ', newline='\r\n')
        np.savetxt('./Result/mat_exist.txt', self.mat_exist, fmt='%d', delimiter=' ', newline='\r\n')
        self.obtain_state(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
        observation = np.copy(self.state)
        # print("initial distance", np.linalg.norm(self.position - self.target))
        # print("environment reset", self.orient, self.speed)
        return observation

    def step(self, action):
        # print('orient={}, speed={}'.format(self.orient, self.speed))
        self.env_step_counter += 1
        position_temp = np.copy(self.position)
        self.orient = np.mod(1.0 / 4.0 * action[0] * np.pi + self.orient, 2 * np.pi)
        self.speed = np.where(action[1] >= 0, self.speed + 2 * action[1] * (-np.tanh(0.5 * (self.speed - 50.0))),
                              self.speed + 2 * action[1] * np.tanh(0.5 * self.speed))

        iter_num = np.where(self.speed / self.min_step > np.floor(self.speed / self.min_step),
                            np.int(self.speed / self.min_step) + 1, np.int(self.speed / self.min_step))[0]
        end_cache = np.copy(self.position)
        end = np.mod(end_cache, self.repeat)
        done1 = False
        for i in range(iter_num):
            position_integer = (end_cache / self.repeat).astype(np.int)
            if self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand] > 0:
                if np.linalg.norm(end - np.array([self.repeat / 2, self.repeat / 2])) - self.radius <= 0:
                    done1 = True
                    break
                end_cache = end_cache + \
                            np.array([self.min_step * np.sin(self.orient[0]), self.min_step * np.cos(self.orient[0])])
                end = np.mod(end_cache, self.repeat)

        if not done1:
            self.position = self.position + \
                            np.array([self.speed[0] * np.sin(self.orient[0]), self.speed[0] * np.cos(self.orient[0])])
        else:
            self.position = end_cache
        done2 = (np.linalg.norm(self.position - self.target) <= 10)
        done3 = self.env_step_counter >= self._max_episode_steps

        # terminal judgement ###########################################################################################
        done = done1 + done2 + done3
        if done1:
            print('agent collides with obstacles!')
        if done2:
            print('agent arrived at the destination!')
        if done3:
            print('environment steps exceed the maximum steps!')

        self.obtain_state(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
        next_observation = np.copy(self.state)

        # Reward #######################################################################################################
        reward = 0
        if self.reward_type == "sparse":
            reward_sparse = np.where(done2, np.zeros([1]) + 1.0, np.zeros([1]))
            reward = reward_sparse[0]
        elif self.reward_type == "dense":
            reward_sparse = np.where(done2, np.zeros([1]) + 10.0, np.zeros([1]))
            # reward_distance = np.tanh(0.2 * (10.0 - self.speed)) * (
            #             np.linalg.norm(position_temp - self.target) - np.linalg.norm(self.position - self.target))

            reward_distance = np.linalg.norm(position_temp - self.target) - np.linalg.norm(self.position - self.target)

            # reward_distance = np.max([np.linalg.norm(position_temp - self.target)
            #                           - np.linalg.norm(self.position - self.target), 0.0])

            reward_barrier = np.where(np.min(self.state[0:9]) * 100 <= 10.0, -5.0 + np.zeros([1]), np.zeros([1]))
            reward_action = -1.5 * np.ones([1])
            reward = (reward_sparse + reward_barrier + reward_distance + reward_action)[0] / 10.0
        else:
            print("Unauthorized reward type! (dense or sparse)")
            exit(0)

        return next_observation, reward, done, done2

    def render(self):
        print('orient={}, speed={}'.format(self.orient, self.speed))
        self.File.write(str(self.target[0]))
        self.File.write(' ')
        self.File.write(str(self.target[1]))
        self.File.write(' ')
        self.File.write(str(self.position[0]))
        self.File.write(' ')
        self.File.write(str(self.position[1]))
        self.File.write(' ')
        for j in range(len(self.state)):
            self.File.write(str(self.state[j]))
            self.File.write(' ')
        self.File.write(str(self.speed[0]))
        self.File.write(' ')
        self.File.write(str(self.orient[0]))
        self.File.write('\n')

    def end(self):
        self.File.close()

    def seed(self, seed):
        np.random.seed(seed)















