import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    types = 'decay'
    # types = 'no_decay'
    sub_types = 'uav'
    # sub_types = 'uav_wrong'

    if types == 'decay':

        if sub_types == 'uav':
            LwH_uav = np.load('LwH_uav.npy')
            A3C_uav = np.load('A3C_uav.npy')
            DDPGfD_uav = np.load('DDPGfD_uav.npy')
            POfD_uav = np.load('POfD_uav.npy')
            data = [LwH_uav, POfD_uav, DDPGfD_uav, A3C_uav]
        else:
            LwH_uav_wrong = np.load('LwH_uav_wrong.npy')
            A3C_uav_wrong = np.load('A3C_uav_wrong.npy')
            DDPGfD_uav_wrong = np.load('DDPGfD_uav_wrong.npy')
            POfD_uav_wrong = np.load('POfD_uav_wrong.npy')
            data = [LwH_uav_wrong, POfD_uav_wrong, DDPGfD_uav_wrong, A3C_uav_wrong]

        label = ['LwH', 'POfD', 'DDPGfD', 'A3C']
        labels_dict = dict(zip(label, data))
    else:
        if sub_types == 'uav':
            LwH_uav = np.load('LwH_uav.npy')
            LwH_uav_decay = np.load('LwH_uav_decay.npy')
            data = [LwH_uav, LwH_uav_decay]
        else:
            LwH_uav = np.load('LwH_uav_wrong.npy')
            LwH_uav_wrong_decay = np.load('LwH_uav_wrong_decay.npy')
            data = [LwH_uav, LwH_uav_wrong_decay]
        label = ['LwH', 'LwH (no prior decay)']
        labels_dict = dict(zip(label, data))

    color = ['red', 'green', 'royalblue', 'darkorange']

    # exit(0)

    # title = e + '_' + d + '_' + p + '_' + 'success_rate'
    figsize = 10, 9.15
    plt.figure(figsize=figsize)
    plt.rcParams.update({'font.size': 35, 'font.family': 'serif', 'font.serif': 'Times New Roman'})
    plt.gca().yaxis.get_major_formatter().set_powerlimits((0, 1))
    plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 1))

    i = 0
    for k in labels_dict.keys():
        plt.plot(labels_dict[k][0], labels_dict[k][1], linewidth=4.0, label=k, color=color[i])
        i += 1



    # plt.ylim([0, 1.1])
    plt.xlim([0, 1e7])
    my_y_ticks = np.arange(0, 1.1, 0.2)
    my_x_ticks = np.arange(0, 1e7+1, 2e6)
    plt.xticks(my_x_ticks)
    plt.yticks(my_y_ticks)

    plt.xlabel('training step')
    plt.ylabel('success rate')

    plt.legend()
    plt.grid(color='dimgray', linewidth=2.0, linestyle='--')
    # plt.grid()
    # plt.title(title)
    # plt.savefig(title)
    plt.show()
    plt.close()
    # exit(0)