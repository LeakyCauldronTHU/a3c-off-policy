import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    # prior_decay = ['prior_decay=0_003', 'prior_decay=0_001', 'prior_decay=0_0005', 'prior_decay=1e-05']
    # variance = ['variance=0_01', 'variance=0_04', 'variance=0_09', 'variance=0_25', 'variance=0_36']
    # demo_type = ['demo_type=uav_2', 'demo_type=uav_wrong']
    # path = r'd:\philly_logs\a3c-off-policy-old'

    # environment = ['uav-v0', 'uav-v1']
    environment = ['uav-v0']
    prior_decay = ['pri_dec=0_001', 'pri_dec=0_0005', 'pri_dec=0_0001', 'pri_dec=5e-05', 'pri_dec=1e-05', 'pri_dec=5e-06']
    variance = ['var=0_01', 'var=0_04', 'var=0_09', 'var=0_16', 'var=0_25']
    # demo_type = ['dem_typ=uav-', 'dem_typ=uav_wrong']
    # demo_type = ['dem_typ=uav-']
    demo_type = ['dem_typ=uav_wrong']
    path = r'c:\d\logs_a3c_off_policy\uav_wrong'

    # plot_type = 'error'
    plot_type = 'success'
    keys = ['var=0_01', 'var=0_04', 'var=0_09', 'var=0_16', 'var=0_25']
    values = ['std=0.1', 'std=0.2', 'std=0.3', 'std=0.4', 'std=0.5']
    labels = dict(zip(keys, values))

    if plot_type == 'success':
        for e in environment:
            for d in demo_type:
                for p in prior_decay:
                    # for v in variance:
                    log_set = []
                    for file in os.listdir(path):
                        print("origin file", file, d, p, e)
                        if d in file and p in file and e in file and 'rew_typ' not in file and 'see=2402' in file:
                            label = ''
                            print("file", file)
                            for v in variance:
                                if v in file:
                                    label = v
                            log_set.append([os.path.join(path, file, e + '_log.txt'), label])

                    # print(log_set)
                    # exit(0)
                    title = e + '_' + d + '_' + p + '_' + 'success_rate'
                    # plt.figure(title)
                    figsize = 8, 7
                    # figure, ax = plt.subplots(figsize=figsize)
                    plt.figure(figsize=figsize)
                    plt.rcParams.update({'font.size': 20, 'font.family': 'serif', 'font.serif': 'Times New Roman'})

                    plt.gca().yaxis.get_major_formatter().set_powerlimits((0, 1))
                    plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 1))
                    for f in range(len(log_set)):
                        data = open(log_set[f][0])
                        label = log_set[f][1]
                        line = data.readline()

                        training_steps = []
                        success_rates = []
                        episode_return = []
                        while line:
                            if "training steps" in line:
                                try:
                                    subline = line.split(',')[3]
                                    step = int(subline.split(' ')[-1])
                                    training_steps.append(step)
                                except:
                                    print(line, log_set[f][0])

                            if "success_rate" in line:
                                try:
                                    subline = line.split(',')[5]
                                    success = float(subline.split(' ')[-1])
                                    success_rates.append(success)
                                except:
                                    print(line, log_set[f][0])

                            if "reward episode" in line:
                                subline = line.split(',')[4]
                                reward = float(subline.split(' ')[-1])
                                episode_return.append(reward)

                            line = data.readline()
                        plt.plot(training_steps, success_rates, label=labels[label], linewidth=3.0)
                        plt.xlabel('training step')
                        plt.ylabel('success rate')
                    plt.legend()
                    plt.grid()
                    plt.title(title)
                    plt.savefig(title)
                    plt.show()
                    plt.close()
                    # exit(0)

    elif plot_type == 'error':
        for e in environment:
            for d in demo_type:
                for p in prior_decay:
                    # for v in variance:
                    log_set = []
                    for file in os.listdir(path):
                        print("origin file", file, d, p, e)
                        if d in file and p in file and e in file and 'rew_typ' not in file:
                            label = ''
                            # print("file", file)
                            for v in variance:
                                if v in file:
                                    label = v
                            log_set.append([os.path.join(path, file, 'TD_Error.txt'), label])

                    # print(log_set)
                    # exit(0)
                    title = e + '_' + d + '_' + p + '_' + 'td_errors'
                    plt.figure(title)
                    for f in range(len(log_set)):
                        data = open(log_set[f][0])
                        label = log_set[f][1]
                        line = data.readline()

                        training_steps = []
                        td_errors = []
                        while line:
                            try:
                                ste = str(int(line.split(' ')[1]))
                                err = float(line.split(' ')[0])
                                training_steps.append(ste)
                                td_errors.append(err)
                            except:
                                print(f, line)
                                pass

                            line = data.readline()

                        data = dict(zip(training_steps, td_errors))
                        training_steps = list(data.keys())
                        td_errors = list(data.values())
                        training_steps = training_steps[::100]
                        td_errors = td_errors[::100]
                        training_steps = [int(step) for step in training_steps]
                        td_errors = [np.log(error) for error in td_errors]

                        plt.plot(training_steps, td_errors, label=label, linewidth=3.0)
                        plt.xlabel('training steps')
                        plt.ylabel('TD errors')
                    plt.legend()
                    plt.grid()
                    plt.title(title)
                    plt.savefig(title)
                    plt.show()
                    plt.close()
                    # exit(0)
    else:
        pass