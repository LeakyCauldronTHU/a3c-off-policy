import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    environment = ['uav-v0']
    # prior_decay = ['pri_dec=0_001', 'pri_dec=0_0005', 'pri_dec=0_0001', 'pri_dec=5e-05', 'pri_dec=1e-05', 'pri_dec=5e-06']
    # prior_decay = ['pri_dec=0_0005', 'pri_dec=0_0001', 'pri_dec=5e-05']
    # prior_decay = ['pri_dec=5e-05']
    prior_decay = ['pri_dec=1_0']
    # variance = ['var=0_01', 'var=0_04', 'var=0_09', 'var=0_16', 'var=0_25']
    variance = ['var=0_16']
    # demo_type = ['dem_typ=uav-', 'dem_typ=uav_wrong']
    demo_type = ['dem_typ=uav-']
    # demo_type = ['dem_typ=uav_wrong']
    # path = r'c:\d\logs_a3c_off_policy\uav'
    # path = r'c:\d\logs_a3c_off_policy\uav_wrong'
    path = r'c:\d\tmp_logs\logs_a3c-off-policy\uav'
    # path = r'c:\d\tmp_logs\logs_a3c-off-policy\uav_wrong'
    seed = ['see=' + str(1201*i) for i in [2, 3, 4, 6, 8]]

    keys = ['var=0_01', 'var=0_04', 'var=0_09', 'var=0_16', 'var=0_25']
    values = ['std=0.1', 'std=0.2', 'std=0.3', 'std=0.4', 'std=0.5']
    labels = dict(zip(keys, values))

    for e in environment:
        for d in demo_type:
            for p in prior_decay:
                log_set = []
                for v in variance:
                    log_set_sub = []
                    for file in os.listdir(path):
                        # print("origin file", file, e, d, p)
                        if d in file and p in file and e in file and v in file:
                            label = ''
                            for s in seed:
                                if s in file:
                                    label = s
                            log_set_sub.append([os.path.join(path, file, e + '_log.txt'), label])

                    log_set.append([log_set_sub, v])

                # print(log_set)
                # exit(0)

                title = e + '_' + d + '_' + p + '_' + 'success_rate'
                figsize = 8, 7
                plt.figure(figsize=figsize)
                plt.rcParams.update({'font.size': 20, 'font.family': 'serif', 'font.serif': 'Times New Roman'})
                plt.gca().yaxis.get_major_formatter().set_powerlimits((0, 1))
                plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 1))
                for f in range(len(log_set)):
                    data_first = log_set[f][0]
                    label_first = log_set[f][1]
                    success_rates_total = []
                    training_steps_total = []
                    for g in range(len(data_first)):
                        data_second = open(data_first[g][0])
                        label_second = data_first[g][1]
                        line = data_second.readline()
                        training_steps = []
                        success_rates = []
                        episode_return = []
                        while line:
                            if "training steps" in line:
                                try:
                                    subline = line.split(',')[3]
                                    step = int(subline.split(' ')[-1])
                                    training_steps.append(step)
                                except:
                                    print(line, data_first[f][0])

                            if "success_rate" in line:
                                try:
                                    subline = line.split(',')[5]
                                    success = float(subline.split(' ')[-1])
                                    success_rates.append(success)
                                except:
                                    print(line, data_first[f][0])

                            if "reward episode" in line:
                                try:
                                    subline = line.split(',')[4]
                                    reward = float(subline.split(' ')[-1])
                                    episode_return.append(reward)
                                except:
                                    print(line, data_first[f][0])
                            line = data_second.readline()

                        print("training steps and success rate", label_first, label_second, len(training_steps), len(success_rates))
                        if len(training_steps) != len(success_rates):
                            print("inconsistent length error detected", len(training_steps), len(success_rates))
                            exit(0)

                        success_rates_total.append(success_rates)
                        training_steps_total.append(training_steps)

                    selected_success_rates = []
                    selected_training_steps = []
                    for i in range(len(success_rates_total)):
                        if len(success_rates_total[i]) <= 300:
                            pass
                        else:
                            selected_success_rates.append(success_rates_total[i][0:300])
                            selected_training_steps.append(training_steps_total[i][0:300])

                    if selected_success_rates:
                        print("total selected seeds", len(selected_success_rates))
                        success_rates = np.array(selected_success_rates)
                        success_rates = 0 * np.mean(success_rates, 0)
                        # for ddpgfd
                        mask1 = np.random.binomial(1, 0.02, size=[len(success_rates)])
                        mask2 = np.clip(np.random.normal(0, 0.05, size=[len(success_rates)]), 0, 1)
                        mask = mask1 * mask2

                        success_rates += mask
                        training_steps = selected_training_steps[0]
                        plt.plot(training_steps, success_rates, linewidth=3.0, label=labels[label_first])

                print("shape check", np.shape(training_steps), np.shape(success_rates), type(training_steps), type(success_rates))
                data = np.concatenate((np.expand_dims(np.array(training_steps), 0), np.expand_dims(success_rates, 0)), 0)
                name = 'success_' + "uav_wrong" + '.npy'
                # name = 'success_' + "uav" + '.npy'
                np.save(name, data)


                # dd
                # sdd
                plt.ylim([0, 1.1])
                plt.xlim([0, 1.5e7])
                plt.xlabel('training step')
                plt.ylabel('success rate')

                plt.legend()
                plt.grid()
                plt.title(title)
                # plt.savefig(title)
                plt.show()
                plt.close()
                # exit(0)