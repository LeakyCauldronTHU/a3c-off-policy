import vtk

# draw a plane
plane_source = vtk.vtkPlaneSource()
plane_source.SetCenter(0, 0, 0)
plane_source.SetNormal(0, 0, 1)
plane_mapper = vtk.vtkPolyDataMapper()
plane_mapper.SetInputConnection(plane_source.GetOutputPort())
plane_actor = vtk.vtkActor()
plane_actor.SetMapper(plane_mapper)

# create render
render = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(render)
renWin.SetSize(800, 600)
renInter = vtk.vtkRenderWindowInteractor()
renInter.SetRenderWindow(renWin)
render.AddActor(plane_actor)
renWin.Render()
renInter.Initialize()

# set callback
def cb(interactor, event):
    global plane_actor
    plane_actor.RotateZ(0.1)
    interactor.GetRenderWindow().Render()

renInter.AddObserver('TimerEvent', cb)
timerId = renInter.CreateRepeatingTimer(100)

# put something here?
renWin = vtk.vtkRenderWindow()
imageFilter = vtk.vtkWindowToImageFilter()
imageFilter.SetInput(renWin)
imageFilter.SetInputBufferTypeToRGB()
imageFilter.ReadFrontBufferOff()
imageFilter.Update()

#Setup movie writer
moviewriter = vtk.vtkOggTheoraWriter()
moviewriter.SetInputConnection(imageFilter.GetOutputPort())
moviewriter.SetFileName("c:/d/movie.avi")
moviewriter.Start()
renWin.Start()

#Export a single frame
imageFilter.Modified()
moviewriter.Write()

#Finish movie
# moviewriter.End()