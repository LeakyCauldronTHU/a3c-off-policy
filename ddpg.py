import numpy as np
import torch
from noise_ddpg import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from model_ddpg import (Actor, Critic)


class DDPG(object):
    def __init__(self, observation_space, action_space, args):

        if args.seed > 0:
            self.seed(args.seed)

        self.state_space = observation_space.shape[0]
        self.action_space = action_space.shape[0]
        self.action_range = action_space.high[0]

        # Create Actor and Critic Network
        net_cfg = {
            'hidden1': args.hidden1,
            'hidden2': args.hidden2,
            'init_w': args.init_w
        }

        self.action_noise = NormalActionNoise(mu=np.zeros(self.action_space),
                                              sigma=float(0.3) * np.ones(self.action_space))
        self.actor = Actor(self.state_space, self.action_space, **net_cfg)
        self.critic = Critic(self.state_space, self.action_space, **net_cfg)
        self.is_training = False

    def eval(self):
        self.actor.eval()
        self.critic.eval()

    def random_action(self):
        action = np.random.uniform(-self.action_range, self.action_range, self.action_space)
        self.a_t = action
        return action

    def select_action(self, s_t, decay_epsilon=True):
        action = (
            self.actor(torch.Tensor(np.array([s_t])))
        ).squeeze(0)
        action = action.data.cpu().numpy()
        noise = self.action_noise()
        action += self.is_training * noise
        action = np.clip(action, -self.action_range, self.action_range)

        return action

    def load_weights(self, output):
        if output is None: return

        self.actor.load_state_dict(
            torch.load('{}/actor.pkl'.format(output))
        )

        self.critic.load_state_dict(
            torch.load('{}/critic.pkl'.format(output))
        )

    # def save_model(self, output):
    #     torch.save(
    #         self.actor.state_dict(),
    #         '{}/actor.pkl'.format(output)
    #     )
    #     torch.save(
    #         self.critic.state_dict(),
    #         '{}/critic.pkl'.format(output)
    #     )

    def seed(self, s):
        torch.manual_seed(s)
