import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    environment = ['uav-v0']
    # prior_decay = ['pri_dec=0_001', 'pri_dec=0_0005', 'pri_dec=0_0001', 'pri_dec=5e-05', 'pri_dec=1e-05', 'pri_dec=5e-06']
    prior_decay = ['pri_dec=0_0005', 'pri_dec=0_0001', 'pri_dec=5e-05']
    variance = ['var=0_01', 'var=0_04', 'var=0_09', 'var=0_16', 'var=0_25']
    # demo_type = ['dem_typ=uav-', 'dem_typ=uav_wrong']
    # demo_type = ['dem_typ=uav-']
    demo_type = ['dem_typ=uav_wrong']
    # path = r'c:\d\logs_a3c_off_policy\uav'
    path = r'c:\d\logs_a3c_off_policy\uav_wrong'
    seed = ['see=' + str(1201*i) for i in [2, 3, 4, 6, 8]]

    keys = ['var=0_01', 'var=0_04', 'var=0_09', 'var=0_16', 'var=0_25']
    values = ['std=0.1', 'std=0.2', 'std=0.3', 'std=0.4', 'std=0.5']
    labels = dict(zip(keys, values))

    for e in environment:
        for d in demo_type:
            for p in prior_decay:
                log_set = []
                for v in variance:
                    log_set_sub = []
                    for file in os.listdir(path):
                        # print("origin file", file, e, d, p)
                        if d in file and p in file and e in file and v in file:
                            label = ''
                            for s in seed:
                                if s in file:
                                    label = s
                            log_set_sub.append([os.path.join(path, file, e + '_log.txt'), label])

                    log_set.append([log_set_sub, v])

                for f in range(len(log_set)):
                    data_first = log_set[f][0]
                    label_first = log_set[f][1]
                    success_rates_total = []
                    training_steps_total = []
                    for g in range(len(data_first)):
                        data_second = open(data_first[g][0])
                        label_second = data_first[g][1]
                        line = data_second.readline()
                        training_steps = []
                        success_rates = []
                        episode_return = []
                        try:
                            while line:
                                if "training steps" in line:
                                    try:
                                        subline = line.split(',')[3]
                                        step = int(subline.split(' ')[-1])
                                        training_steps.append(step)
                                    except:
                                        print(line, data_first[f][0])

                                if "success_rate" in line:
                                    try:
                                        subline = line.split(',')[5]
                                        success = float(subline.split(' ')[-1])
                                        success_rates.append(success)
                                    except:
                                        print(line, data_first[f][0])

                                if "reward episode" in line:
                                    try:
                                        subline = line.split(',')[4]
                                        reward = float(subline.split(' ')[-1])
                                        episode_return.append(reward)
                                    except:
                                        print(line, data_first[f][0])
                                line = data_second.readline()

                            print("training steps and success rate", label_first, label_second, len(training_steps), len(success_rates))
                            if len(training_steps) != len(success_rates):
                                print("inconsistent length error detected", len(training_steps), len(success_rates))
                                exit(0)

                            lost_items = []
                            for i in range(len(training_steps) - 1):
                                if training_steps[i+1] != training_steps[i] + 50000:
                                    lost_items.append(training_steps[i+1])

                            print("lost items", data_first[g][0], lost_items)
                        except:
                            print("the log file is problematic, please check it", data_first[g][0])

