from __future__ import division
import math
import numpy as np
import torch
from torch.autograd import Variable
from utils import normal
from policy_domain import Policy_Domain


class Agent(object):
    def __init__(self, model, env, args, state, rank):
        self.time_step = 0
        self.model = model
        self.env = env
        self.state = state
        self.eps_len = 0
        self.args = args
        self.values = []
        self.log_probs = []
        self.rewards = []
        self.infos = []
        self.entropies = []
        self.done = True
        self.reward = 0
        self.info = None
        self.rank = rank
        self.action_pre = []
        self.action_pre_sup = []
        self.reset_flag = False
        self.action_test_collection = []

        self.prior = Policy_Domain(env.observation_space, env.action_space)

    def action_pre_train(self):
        # supervised training of action and dynamic programming of value
        # if self.env.name == 'UAV':
        #     mu_prior, sigma_prior = self.prior.forward_UAV(Variable(self.state), 1, self.args)
        # else:
        #     mu_prior, sigma_prior = self.prior.forward(Variable(self.state), 1, self.args)

        mu_prior, sigma_prior = self.prior.forward(Variable(self.state), 1, self.args)

        value, mu, sigma = self.model(Variable(self.state))
        mu_prior = torch.clamp(mu_prior, self.env.action_space.low[0], self.env.action_space.high[0])

        eps = torch.randn(mu_prior.size())
        eps = Variable(eps)
        action = (mu_prior + sigma_prior.sqrt() * eps).data

        action = torch.clamp(action, self.env.action_space.low[0], self.env.action_space.high[0])
        state, reward, self.done, self.info = self.env.step(action.cpu().numpy())

        self.action_pre.append([mu, sigma])
        self.action_pre_sup.append([mu_prior.data, sigma_prior.data])

        self.state = torch.from_numpy(state).float()

        self.eps_len += 1
        self.done = self.done
        self.values.append(value)
        self.rewards.append(reward)
        self.infos.append(self.info)

    def action_train(self):
        self.time_step += 1
        if self.args.no_decay:
            self.time_step = 1
        value, mu_learned, sigma_learned = self.model(Variable(self.state))

        if self.args.use_prior:
            # print("self.timesteps", self.time_step)
            mu_prior, sigma_prior = self.prior.forward(Variable(self.state), self.time_step, self.args)
            sigma_prior = sigma_prior.diag()

        sigma_learned = sigma_learned.diag()

        self.reset_flag = False

        if self.args.use_prior:
            sigma = (sigma_learned.inverse() + sigma_prior.inverse()).inverse()
            temp = torch.matmul(sigma_learned.inverse(), mu_learned) + torch.matmul(sigma_prior.inverse(), mu_prior)
            mu = torch.matmul(sigma, temp)
        else:
            sigma = sigma_learned
            mu = mu_learned
        
        sigma = sigma.diag()
        sigma_learned = sigma_learned.diag()

        eps = torch.randn(mu.size())
        pi = np.array([math.pi])
        pi = torch.from_numpy(pi).float()
        eps = Variable(eps)
        pi = Variable(pi)

        action = (mu + sigma.sqrt() * eps).data

        # if self.rank == 1:
        #     print('mu={}, sigma_learned={}, sigma_prior={}, action={}'
        #           .format(mu.data, sigma_learned.data, sigma_prior.data, action))
        act = Variable(action)
        prob = normal(act, mu, sigma)
        action = torch.clamp(action, self.env.action_space.low[0], self.env.action_space.high[0])
        entropy = 0.5 * ((sigma_learned * 2 * pi.expand_as(sigma_learned)).log() + 1)
        self.entropies.append(entropy)
        log_prob = (prob + 1e-6).log()
        self.log_probs.append(log_prob)
        state, reward, self.done, self.info = self.env.step(action.cpu().numpy())

        # if self.rank==1:
        #     self.env.render()

        # reward = max(min(float(reward), 1.0), -1.0)
        self.state = torch.from_numpy(state).float()
        self.eps_len += 1
        self.done = self.done

        self.values.append(value)
        self.rewards.append(reward)
        self.infos.append(self.info)
        return self

    def action_test(self):
        with torch.no_grad():
            value, mu, sigma = self.model(Variable(self.state))
        action = mu.data.cpu().numpy()

        # mu, sigma = self.prior.forward(Variable(self.state), 1, self.args, False)
        # eps = torch.randn(mu.size())
        # eps = Variable(eps)
        # action = (mu + sigma.sqrt() * eps).data.cpu().numpy()

        # action = torch.clamp(action, self.env.action_space.low[0], self.env.action_space.high[0])
        # action = action.data.cpu().numpy()

        self.action_test_collection.append(action)
        # action = [0.1, 0.1]
        state, self.reward, self.done, self.info = self.env.step(action)
        # print(self.reward, self.done, info)
        # self.env.render()

        self.state = torch.from_numpy(state).float()
        self.eps_len += 1
        self.done = self.done or self.eps_len >= self.args.max_episode_length
        return self

    def clear_actions(self):
        self.values = []
        self.log_probs = []
        self.rewards = []
        self.entropies = []
        self.action_pre = []
        self.action_pre_sup = []
        return self
